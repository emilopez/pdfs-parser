#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 24 10:12:33 2017

@author: emiliano lopez - emiliano [dot] lopez [at] gmail [dot] com
@requirements: 
    - Python 3.6.1, 
    - PyPDF2 1.26.0 (https://pypi.python.org/pypi/PyPDF2/1.26.0)
    
@doc: https://gitlab.com/emilopez/pdfs-parser


"""

import os
from datetime import datetime
import argparse

from PyPDF2 import PdfFileWriter
from funciones import recortar, obtener_recibos



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-izq", type=int, nargs='?', const=1, default=-25, help="margen izquierdo")
    parser.add_argument("-der", type=int, nargs='?', const=1, default=120, help="margen derecho")
    parser.add_argument("-sup", type=int, nargs='?', const=1, default=-25, help="margen superior")
    parser.add_argument("-inf", type=int, nargs='?', const=1, default=0, help="margen inferior")
    parser.add_argument("-archivo_entrada", nargs='?', const=1, default="test.PDF", help="archivo de todos los recibos")
    parser.add_argument("-archivo_salida_recortado", nargs='?', const=1, default="default", help="archivo recortado")
    parser.add_argument("-directorio_dst_recibos", nargs='?', const=1, default="default", help="directorio destino de los recibos PDF individualizados")
    parser.add_argument("-archivo_entrada_recibos_recortados", nargs='?', const=1, default="default", help="archivo a partir del cual extraer los recibos individuales")
    parser.add_argument("-duplicar", type=int, nargs='?', const=1, default=0, help="duplica el archivo")
        
    args = parser.parse_args()
    
    # margenes y archivo de entrada
    left, top, right, bottom = args.izq, args.sup, args.der, args.inf
    src_filename = args.archivo_entrada
    
    # Nombre para el archivo recortado, x defecto: YYYY-MM-DD_HHMMSS_recortado.pdf'
    dst_filename = args.archivo_salida_recortado
    if dst_filename == 'default':
        dst_filename = str(datetime.now())[:19].replace(' ', '_').replace(':','') + '_recortado.pdf'
    
    # Nombre de archivo de entrada ya recortado desde donde se generan
    # los pdfs de los recibos individuales, x defecto el que se genero en proceso previo: dst_filename
    input_file = args.archivo_entrada_recibos_recortados
    if input_file == 'default':
        input_file = dst_filename
    
    # Si es 0 no duplica
    duplicar = args.duplicar
    
    # 1- Genera PDF RECORTADO
    print("+ Generando PDF recortado...")
    print("  Archivo entrada: {}".format(src_filename))
    print("  Archivo a generar: {}".format(dst_filename))
    print()
    recortar(src_filename, dst_filename, right, top, left, bottom)
    
    # 2- Genera diccionario de objetos pages
    print("+ Individualizando los recibos: ", end='')
    datos = obtener_recibos(input_file)
    print(len(datos['recibos']), "recibos por generar")
    if duplicar:
        for nro_legajo, paginas in datos["recibos"].items():
            datos["recibos"][nro_legajo] += paginas
    
    # 3- Directorio destino donde poner los pdfs de recibos individuales
    if args.directorio_dst_recibos == "default":
        dst_dir = str(datetime.now())[:19].replace(' ', '_').replace(':','') + '_' + datos["escuela"]
    os.makedirs(dst_dir)
    
    print(" +------------+------------+")
    print(" |{0:^11} | {1:^11}|".format("Legajo","Páginas"))
    print(" +------------+------------+")
    for nro_legajo, paginas in datos["recibos"].items():
        print(" |{0:>11} | {1:^11}|".format(nro_legajo, len(paginas)))
        pdf_recibo_personal = PdfFileWriter()
        for pagina in paginas:
            pdf_recibo_personal.addPage(pagina)
        archivo_recibo_personal = open(os.path.join(dst_dir, nro_legajo.replace("/","-")+'.pdf'), 'wb')
        pdf_recibo_personal.write(archivo_recibo_personal)
        archivo_recibo_personal.close()
    print(" +------------+------------+")
    print()
    print("PDFs guardados en carpeta '{}'".format(dst_dir))
    input("<enter> para finalizar")

if __name__ == '__main__':
    main()
