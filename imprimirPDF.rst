Imprimir en un PDF desde un programa que lo hace solamente en impresora por el puerto paralelo
==============================================================================================

Tenés un software obsoleto que funciona sobre DOS y que imprime solamente en una
impresora física por el puerto LPT1 y no permite configurarlo.

Lo que acá hacemos es redirigir ese tráfico que va hacia el puerto y mandarlo a 
una impresora por software que genera archivos PDFs.

Requerimientos
--------------

- DOSPrinter http://dosprinter.net/DOSPrinter.zip
- Windows XP

Procedimiento de instalación y configuración
--------------------------------------------

1. Descargar DOSPrinter http://dosprinter.net/DOSPrinter.zip

2. Descomprimir el contenido en la carpeta c:\windows .

3. Crear un archivo batch con la siguiente línea: @start DOSPrinter.exe /pdf /TRAY c:\test.prn

4. Instalar una Impresora Generica/de solo texto. Pasos:

- Click Inicio / Configuración / Impresoras / Agregar impresora. Seleccionar Impresora Generica/de solo texto.

- Click derecho sobre la impresora creada. Ir a Propiedades / Puertos / Agregar Puerto / Elegir Puerto Local / Click Nuevo Puerto / Poner el nombre de puerto: "C:\TEST.PRN" (sin las comillas). Click Cerrar.

5. Compartirla. Pasos:

- Click derecho nuevamente sobre el ícono de la impresora. Click en Compartir. Luego en Compartir como. Ingresar el nombre de la impresora, algo como "prn" (sin las comillas).

- En la línea de comandos de tu windows, hacé lo siguiente: 

    ``NET USE LPT1: \\NombrePC\prn /persistent:yes ``
    
    Donde NombrePC es el nombre de tu computadora. Si cometiste un error en el comando previo podés eliminarlo haciendo:
    
    ``net use lpt1: /delete``

6. Antes de usarla tenés que ejecutar el batch del paso 3, o bien hacer que se inicie automáticamente con el sistema operativo.


Posibles errores
----------------

``net use and system error 1231``
Vas a necesitar el cd de windows xp.
 
1. Go to Control Panel
2. Open Add Hardware
3. Click Next
4. After Scan Finishes, select "Yes, I have already connected the hardware"
5. Click Next
6. Scroll to bottom of list and select "Add a new hardware device", Click Next
7. Select "Install the hardware that I manually select from a list (Advanced), click next
8. Select "Network Adapters", click next
9. Select "Microsoft" for Manufacturer
10. Select "Mircosoft Loopback Adapter" in the Network Adapter List, click next, click next again, and click finish.

