#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 26 10:56:26 2017

@author: emiliano
"""
import re

from PyPDF2 import PdfFileWriter, PdfFileReader

def recortar(src_filename, dst_filename, right, top, left, bottom):
    """ Recorta archivo pdf src y lo guarda en archivo dst
    
        Los margenes son: right, top, left, bottom
        Si son negativos entonces agranda el margen.
    """
    pdf = PdfFileReader(open(src_filename, 'rb'))
    pdf_cropped = PdfFileWriter()
    for page in pdf.pages:
        page.mediaBox.upperRight = (page.mediaBox.getUpperRight_x() - right, 
                                    page.mediaBox.getUpperRight_y() - top)
        page.mediaBox.lowerLeft  = (page.mediaBox.getLowerLeft_x()  + left,  
                                    page.mediaBox.getLowerLeft_y()  + bottom)
        pdf_cropped.addPage(page) 
    file_pfd_cropped = open(dst_filename, 'wb')
    pdf_cropped.write(file_pfd_cropped)
    file_pfd_cropped.close()
    
def obtener_nro_legajo(texto):
    """ Parsea el texto retornando el número de legajo
    
        La primer ocurrecia que coincida con la expresion regular es
        el número de legajo. Su forma es x/y, donde x e y son números.
        Este tipo de valor se presenta varias veces por recibo
        pero la primera coincidencia es el número de legajo
        
    """
    for i, field in enumerate(texto.split()):
        if re.findall(r'[0-9]\/[0-9]',field):
            #print(field, texto.split()[i])
            return texto.split()[i]

def obtener_recibos(input_file):
    """ Retorna diccionario con nombre de escuela y recibos
        
        Retorna {"escuela":escuela, "recibos":recibos}
        Donde recibos es un diccionario que usa el nro de legajo como clave: 
        - Las claves son el numero de legajo ("AB/XY")
        - Los valores son una lista con los objetos paginas (https://pythonhosted.org/PyPDF2/PageObject.html)
        - Ejemplo {"40/10": [page1], "42/6": [page1,page2],}
    """
    pdf_cropped = PdfFileReader(open(input_file, 'rb'))
    #print("Paginas: {}".format(pdf_cropped.getNumPages()))
    recibos = {}
    for page in pdf_cropped.pages:
        texto = page.extractText()
        nro_legajo = obtener_nro_legajo(texto)
        escuela = texto.split(":")[1].replace(' ','')[:9]
        if recibos.get(nro_legajo):
            recibos[nro_legajo].append(page)
        else:
            recibos[nro_legajo] = [page]
    return {"escuela":escuela, "recibos":recibos}
