Autor: emiliano lopez - emiliano.lopez [AT] gmail [DOT] com

En esta ayuda se explica el modo de utilizar dos scripts que procesan archivos
PDFs generados por una impresora virtual que captura la salida de un programa
en DOS y en vez de mandarlos al puerto LPT correspondiente, los digitaliza.

Para conocer mas sobre el proceso que los genera ver: https://gitlab.com/emilopez/pdfs-parser/blob/master/imprimirPDF.rst


Requerimientos
--------------

- Python 3.6.1 (https://www.python.org/downloads/release/python-361/)
- PyPDF2 1.26.0 (https://pypi.python.org/pypi/PyPDF2/1.26.0)

Descripción
-----------

Hay dos scripts:

- ``generarRS.py``: a partir de un único PDF con todos los recibos de sueldo
  genera N archivos PDFs, uno por cada persona.
        
- ``agregaMargen.py``: a partir de un archivo de listados, agrega márgenes.

   
Funcionamiento generarRS.py
---------------------------

A continuación se explica la dinámica de ejecución del programa:

1. Recorta margenes del PDF original (por defecto: TEST.PDF) para sacar la 
    basura y genera otro pdf limpio (por defecto: TEST-recortado.PDF)
    
2. Extrae texto de cada pagina del PDF generado en el paso previo detectando el 
    numero de legajo y nombre de la escuela. 
    Con eso se arma un diccionario {"escuela":escuela, "recibos":recibos} donde
    escuela es un string que contiene 9 caracteres a partir del primer número
    de la escuela y recibos es a su vez otro diccionario donde el numero de 
    legajo es la clave, y el valor es una lista con los objetos páginas 
    (https://pythonhosted.org/PyPDF2/PageObject.html)
    de la biblioteca PyPDF2. En resumen:
    - Ejemplo ``{"40/10": [page1], "42/6": [page1,page2], }``
      
    Los archivos PDFs individuales se almacenan en un directorio, por defecto: yyyy-mm-ddESCUELA, y 
    el nombre de archivo de cada recibo es el numero de legajo reemplazando la barra 
    por un guion, por ejemplo de 44/10 -> 44-10.pdf
    

Ejecución
'''''''''

El siguiente ejemplo usa todos los valores por defecto, pero usa como fuente para crear
los recibos de sueldos individuales un archivo específico, en este caso ``1201-cropped-tested.pdf``:

``python generarRS.py -archivo_entrada_recibos_recortados 1201-cropped-tested.pdf``

A continuación la salida de ``python generarRS.py -h``

.. code:: bash
    
    python generarRS.py -h
    usage: generarRS.py [-h] [-izq [IZQ]] [-der [DER]] [-sup [SUP]] [-inf [INF]]
                        [-archivo_origen [ARCHIVO_ORIGEN]]
                        [-archivo_salida_recortado [ARCHIVO_SALIDA_RECORTADO]]
                        [-directorio_recibos [DIRECTORIO_RECIBOS]]
                        [-archivo_entrada_recibos_recortados [ARCHIVO_ENTRADA_RECIBOS_RECORTADOS]]

    optional arguments:
      -h, --help            show this help message and exit
      -izq [IZQ]            margen izquierdo
      -der [DER]            margen derecho
      -sup [SUP]            margen superior
      -inf [INF]            margen inferior
      -archivo_origen [ARCHIVO_ORIGEN]
                            archivo de todos los recibos
      -archivo_salida_recortado [ARCHIVO_SALIDA_RECORTADO]
                            archivo recortado
      -directorio_recibos [DIRECTORIO_RECIBOS]
                            directorio destino de los recibos PDF individualizados
      -archivo_entrada_recibos_recortados [ARCHIVO_ENTRADA_RECIBOS_RECORTADOS]
                            archivo a partir del cual extraer los recibos
                            individuales

Funcionamiento agregaMargen.py
------------------------------

Muy sencillo, se le dice el archivo de origen, la cantidad que se desea recortar cada margen
y el nombre de archivo destino. 

``python generarRS.py -archivo_entrada listado.pdf -izq 100 -der 100 -sup 100 -inf 100 -archivo_salida listadoMargen.pdf``


