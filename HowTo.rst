Imprimir en pdf desde DOS
=========================

- Iniciar la máquina virtual usando virtual box

- Una vez iniciado windows xp, se debe corroborar que se encuentre iniciada la impresora virtual, esto aparece en un ícono al lado del reloj de la barra de tareas. En caso que no se encuentre, se debe ejecutar el archivo c:\StartPrinter.bat

- Luego, cualquier programa que intente imprimir a través del puerto LPT1 será redirigido a esta impresora virtual generando el archivo c:\test.PDF. Es importante renombrar el archivo o moverlo de lugar para que no sea reemplazado por una nueva impresión.

- Si se desea generar un PDF que utilice una plantilla de fondo, se debe abrir el programa de la impresora virtual (doble click en el ícono) y bajo la pestaña Watermark, ingresar la ruta del archivo, por ejemplo c:\ESACOP6.jpg e ingresando una resolución en el campo Image Resolution DPI (por ejemplo 100).

- Es importante destacar que el cambio previo se hace permanente, por lo que al generar todo pdf va a utilziar esa plantilla a menos que lo volvamos a dejar en blanco.

Agregar márgenes a PDFs
'''''''''''''''''''''''

Simplemente ejecutar el programa generarMargen.bat.
Por defecto este programa usa como entrada el archivo c:\test.PDF

En caso que se desee aplicar a un archivo arbitrario se debe ir al menú inicio - ejecutar - ingresar cmd y <enter>. Esto abrirá una pantalla de DOS.

Ahí se debe escribir el comando: ``python c:\agregaMargen.py -archivo_entrada ARCHIVOARBITRARIO.PDF``


Generar recibos de sueldo individuales
''''''''''''''''''''''''''''''''''''''

Similar al caso previo, al ejecutar el archivo ``generarRecibos.bat`` se utiliza por defecto el archivo ``c:\test.PDF``.
Esto genera un archivo recortado cuyo nombre dependerá de la fecha y hora y, el nombre de la escuela que a su vez será utilizado para extraer los recibos en forma individual.

Ahora bien, si se desea utilizar otro archivo de entrada se debe ejecutar desde una pantalla DOS el siguiente comando:

``python c:\agregaMargen.py -archivo_entrada ARCHIVOARBITRARIO.PDF``
