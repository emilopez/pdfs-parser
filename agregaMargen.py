#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 26 16:54:18 2017

@author: emiliano
"""

import argparse
from funciones import recortar

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-izq", type=int, nargs='?', const=1, default=-10, help="margen izquierdo")
    parser.add_argument("-der", type=int, nargs='?', const=1, default=0, help="margen derecho")
    parser.add_argument("-sup", type=int, nargs='?', const=1, default=-10, help="margen superior")
    parser.add_argument("-inf", type=int, nargs='?', const=1, default=-10, help="margen inferior")
    parser.add_argument("-archivo_entrada", nargs='?', const=1, default="TEST.PDF", help="archivo de todos los recibos")
    parser.add_argument("-archivo_salidao", nargs='?', const=1, default="TEST-conMargen.PDF", help="archivo recortado")
    
    args = parser.parse_args()
    
    left, top, right, bottom = args.izq, args.sup, args.der, args.inf
    src_filename = args.archivo_entrada
    dst_filename = args.archivo_salidao
    
    
    # 1- Genera PDF RECORTADO
    print("+ Agregando margen...")
    print("  Archivo entrada: {src}".format(src_filename))
    print("  Archivo salida: {out}".format(dst_filename))
    print()
    recortar(src_filename, dst_filename, right, top, left, bottom)

    input("<enter> para finalizar")
    
if __name__ == '__main__':
    main()
